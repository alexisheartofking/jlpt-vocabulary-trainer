﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
	[SerializeField]
	private Text timerText;
	[SerializeField]
	private Image timerImage;

	[Header("Delegate")]
	public UnityEvent OnTimerDone;

	private bool updateTimer = false;
	private bool useTimer = false;

	private float currentTimer = 0f;
	private float time = 0f;

	private float questionTime = 0f;

	public void Initialize()
	{
		useTimer = Izanagi.Instance.GameSettings.UseTimer;
		timerImage.gameObject.SetActive(useTimer);
		updateTimer = gameObject.activeSelf;

		time = Izanagi.Instance.GameSettings.TotalTimer;
		currentTimer = time;

		questionTime = 0f;

		UpdateUI();
	}

	public void Reset()
	{
		currentTimer = time;
		UpdateUI();
		updateTimer = true;
	}

	public void SetTimerState(bool isPause)
	{
		updateTimer = !isPause;
	}

	public void Update()
	{
		if (updateTimer)
		{
			questionTime += Time.deltaTime;

			if (useTimer)
			{
				currentTimer -= Time.deltaTime;
				if (currentTimer <= 0f)
				{
					currentTimer = 0f;
					updateTimer = false;
					OnTimerDone.Invoke();
				}
				UpdateUI();
			}
		}
	}

	public float GetQuestionTime()
	{
		float tmpQuestionTime = questionTime;
		questionTime = 0f;

		return tmpQuestionTime;
	}

	private void UpdateUI()
	{
		if (currentTimer <= 0f)
			timerText.text = "0";
		else
			timerText.text = Mathf.Min((int)currentTimer + 1, (int)time).ToString();

		timerImage.fillAmount = currentTimer / time;

		if (timerImage.fillAmount <= 0.25f)
			timerImage.color = Color.red;
		else
			timerImage.color = new Color(0, 165f/255f, 1f);
	}
}
