﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : Menu
{
	[Header("Timer")]
	[SerializeField]
	private GameTimer gameTimer;

	[Header("Question")]
	[SerializeField]
	private Text questionCountText;
	[SerializeField]
	private Text questionText;

	[Header("Multiple Choice")]
	[SerializeField]
	private GameObject multipleChoiceParent;
	[SerializeField]
	private GameObject firstLineButton;
	[SerializeField]
	private GameObject secondeLineButton;
	[SerializeField]
	private List<Text> multipleChoiceButtonsText;

	[Header("Multiple Choice")]
	[SerializeField]
	private GameObject keyboardChoiceParent;
	[SerializeField]
	private Transform keyboardPrefab;
	[SerializeField]
	private Transform keyboardParent;
	[SerializeField]
	private Text keyboardAnswerText;

	[Header("Pause Menu")]
	[SerializeField]
	private GameObject pauseMenu;

	private QuestionType currentQuestionType;
	private AnswerType currentAnswerType;

	private List<Datas> AllDatasUsed;
	private Datas currentAnswer;
	private string answerString;

	private int keyPerLine = 8;
	private List<Transform> keyboardKeys;

	private int currentQuestionCount = 0;

	private bool gameIsOver = false;
	private bool transitionToNextQuestion = false;

	private void Start()
	{
		/*Izanagi.Instance.SettingsType = SettingsMenu.Type.TRAINING;
		Izanagi.Instance.GameSettings.TotalTimer = 500;
		Izanagi.Instance.GameSettings.UseTimer = true;

		Izanagi.Instance.GameSettings.QuestionTypes.Add(QuestionType.KANJI_TO_KANA);
		Izanagi.Instance.GameSettings.QuestionTypes.Add(QuestionType.KANA_TO_KANJI);
		Izanagi.Instance.GameSettings.QuestionTypes.Add(QuestionType.ENGLISH_TO_KANJI);
		Izanagi.Instance.GameSettings.QuestionTypes.Add(QuestionType.ENGLISH_TO_KANA);
		Izanagi.Instance.GameSettings.QuestionTypes.Add(QuestionType.KANJI_TO_ROMAJI);

		Izanagi.Instance.GameSettings.AnswerTypes.Add(AnswerType.QUARTET_CHOICES);
		Izanagi.Instance.GameSettings.AnswerTypes.Add(AnswerType.DUAL_CHOICES);
		Izanagi.Instance.GameSettings.AnswerTypes.Add(AnswerType.KEYBOARD);

		Izanagi.Instance.GameSettings.QuestionCount = 2;

		Izanagi.Instance.GameSettings.AllDatas.Add(new Datas(
			0,
			"",
			"開ける/明ける",
			"あける",
			"akeru",
			"",
			"ouvrir",
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
		Izanagi.Instance.GameSettings.AllDatas.Add(new Datas(
			0,
			"",
			"余り",
			"あんまり/あまり",
			"amari/anmari",
			"",
			"excès, trop plein, surplus",
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

		Open();
		LateOpen();*/
	}

	public override void Initialize()
	{
		base.Initialize();
		gameTimer.OnTimerDone.AddListener(TimerDone);
		gameTimer.Initialize();
		keyboardKeys = new List<Transform>();
	}

	public override void Open()
	{
		base.Open();

		Izanagi.Instance.GameSettings.AllRecapDatas = new List<RecapData>();

		AllDatasUsed = Izanagi.Instance.GameSettings.AllDatas;
		currentQuestionCount = 1;
		questionCountText.text = "";
		keyboardAnswerText.text = "";
		gameIsOver = false;

		gameTimer.Initialize();
		gameTimer.SetTimerState(true);

		currentAnswer = AllDatasUsed[Random.Range(0, AllDatasUsed.Count)];
		RandomizeTypes();
		answerString = Izanagi.GetRealAnswerString(currentQuestionType, currentAnswer);
		SetupAnswerType();
		SetupQuestion();
	}

	public override void LateOpen()
	{
		gameTimer.SetTimerState(false);
	}

	private void RandomizeTypes()
	{
		currentQuestionType = RandomizeQuestionType();
		currentAnswerType = RandomizeAnswerType();
	}

	private List<QuestionType> GetQuestionTypeUnavailable()
	{
		List<QuestionType> questionTypeUnavailable = new List<QuestionType>();

		if (currentAnswer.Kanji.Count == 0)
		{
			questionTypeUnavailable.Add(QuestionType.KANJI_TO_KANA);
			questionTypeUnavailable.Add(QuestionType.KANJI_TO_ROMAJI);
			questionTypeUnavailable.Add(QuestionType.KANJI_TO_ENGLISH);

			questionTypeUnavailable.Add(QuestionType.KANA_TO_KANJI);
			questionTypeUnavailable.Add(QuestionType.ROMAJI_TO_KANJI);
			questionTypeUnavailable.Add(QuestionType.ENGLISH_TO_KANJI);
		}
		if (currentAnswer.Kana.Count == 0)
		{
			if (!questionTypeUnavailable.Contains(QuestionType.KANA_TO_KANJI))
				questionTypeUnavailable.Add(QuestionType.KANA_TO_KANJI);

			questionTypeUnavailable.Add(QuestionType.KANA_TO_ROMAJI);
			questionTypeUnavailable.Add(QuestionType.KANA_TO_ENGLISH);

			if (!questionTypeUnavailable.Contains(QuestionType.KANJI_TO_KANA))
				questionTypeUnavailable.Add(QuestionType.KANJI_TO_KANA);

			questionTypeUnavailable.Add(QuestionType.ROMAJI_TO_KANA);
			questionTypeUnavailable.Add(QuestionType.ENGLISH_TO_KANA);
		}
		if (currentAnswer.Romaji.Count == 0)
		{
			if (!questionTypeUnavailable.Contains(QuestionType.ROMAJI_TO_KANJI))
				questionTypeUnavailable.Add(QuestionType.ROMAJI_TO_KANJI);
			if (!questionTypeUnavailable.Contains(QuestionType.ROMAJI_TO_KANA))
				questionTypeUnavailable.Add(QuestionType.ROMAJI_TO_KANA);

			questionTypeUnavailable.Add(QuestionType.ROMAJI_TO_ENGLISH);

			if (!questionTypeUnavailable.Contains(QuestionType.KANJI_TO_ROMAJI))
				questionTypeUnavailable.Add(QuestionType.KANJI_TO_ROMAJI);
			if (!questionTypeUnavailable.Contains(QuestionType.KANA_TO_ROMAJI))
				questionTypeUnavailable.Add(QuestionType.KANA_TO_ROMAJI);

			questionTypeUnavailable.Add(QuestionType.ENGLISH_TO_ROMAJI);
		}
		if (currentAnswer.GetCurrentTranslation() == string.Empty)
		{
			if (!questionTypeUnavailable.Contains(QuestionType.ENGLISH_TO_KANJI))
				questionTypeUnavailable.Add(QuestionType.ENGLISH_TO_KANJI);
			if (!questionTypeUnavailable.Contains(QuestionType.ENGLISH_TO_KANA))
				questionTypeUnavailable.Add(QuestionType.ENGLISH_TO_KANA);
			if (!questionTypeUnavailable.Contains(QuestionType.ENGLISH_TO_ROMAJI))
				questionTypeUnavailable.Add(QuestionType.ENGLISH_TO_ROMAJI);

			if (!questionTypeUnavailable.Contains(QuestionType.KANJI_TO_ENGLISH))
				questionTypeUnavailable.Add(QuestionType.KANJI_TO_ENGLISH);
			if (!questionTypeUnavailable.Contains(QuestionType.KANA_TO_ENGLISH))
				questionTypeUnavailable.Add(QuestionType.KANA_TO_ENGLISH);
			if (!questionTypeUnavailable.Contains(QuestionType.ROMAJI_TO_ENGLISH))
				questionTypeUnavailable.Add(QuestionType.ROMAJI_TO_ENGLISH);
		}

		return questionTypeUnavailable;
	}

	private QuestionType RandomizeQuestionType()
	{
		List<QuestionType> questionTypeUnavailable = GetQuestionTypeUnavailable();
		List<QuestionType> questionTypes = Izanagi.Instance.GameSettings.QuestionTypes;

		for (int i = 0; i < questionTypeUnavailable.Count; ++i)
			questionTypes.Remove(questionTypeUnavailable[i]);

		return questionTypes[Random.Range(0, questionTypes.Count)];
	}

	private List<AnswerType> GetAnswerTypeUnavailable()
	{
		List<AnswerType> answerTypeUnavailable = new List<AnswerType>();

		switch (currentQuestionType)
		{
			case QuestionType.KANJI_TO_ENGLISH:
			case QuestionType.KANA_TO_ENGLISH:
			case QuestionType.ROMAJI_TO_ENGLISH:
				answerTypeUnavailable.Add(AnswerType.KEYBOARD);
				break;
			default:
				break;
		}

		return answerTypeUnavailable;
	}

	private AnswerType RandomizeAnswerType()
	{
		List<AnswerType> answerTypeUnavailable = GetAnswerTypeUnavailable();
		List<AnswerType> answerTypes = Izanagi.Instance.GameSettings.AnswerTypes;

		for (int i = 0; i < answerTypeUnavailable.Count; ++i)
			answerTypes.Remove(answerTypeUnavailable[i]);

		return answerTypes[Random.Range(0, answerTypes.Count)];
	}

	private void SetupAnswerType()
	{
		switch(currentAnswerType)
		{
			case AnswerType.DUAL_CHOICES:
				multipleChoiceParent.SetActive(true);
				firstLineButton.SetActive(true);
				secondeLineButton.SetActive(false);

				keyboardChoiceParent.SetActive(false);

				FillMultipleChoice(2);
				break;
			case AnswerType.KEYBOARD:
				multipleChoiceParent.SetActive(false);
				keyboardChoiceParent.SetActive(true);

				FillKeyboard();
				break;
			case AnswerType.QUARTET_CHOICES:
				multipleChoiceParent.SetActive(true);
				firstLineButton.SetActive(true);
				secondeLineButton.SetActive(true);

				keyboardChoiceParent.SetActive(false);

				FillMultipleChoice(4);
				break;
			default:
				break;
		};
	}

	private void SetupQuestion()
	{
		questionText.text = Izanagi.GetQuestionString(currentQuestionType, currentAnswer);

		if (Izanagi.Instance.GameSettings.QuestionCount > 0)
			questionCountText.text = currentQuestionCount.ToString() + "/" + Izanagi.Instance.GameSettings.QuestionCount;
		else
			questionCountText.text = currentQuestionCount.ToString();
	}

	private void FillKeyboard()
	{
		int lineCount = Mathf.Max(2, (answerString.Length + 2) / keyPerLine);

		foreach (Transform child in keyboardParent)
		{
			Destroy(child.gameObject);
		}

		keyboardKeys.Clear();

		List<int> randomNumbers = new List<int>();

		if (Izanagi.Instance.SettingsType == SettingsMenu.Type.FAST_KANA)
		{
			randomNumbers.Add(Random.Range(0, lineCount * keyPerLine));
		}
		else
		{
			do
			{
				int random = Random.Range(0, lineCount * keyPerLine);

				if (!randomNumbers.Contains(random))
					randomNumbers.Add(random);
			}
			while (randomNumbers.Count < answerString.Length);
		}

		for (int i = 0; i < lineCount * keyPerLine; ++i)
		{
			int tmpIndex = i;
			Transform entry = GameObject.Instantiate(keyboardPrefab, keyboardParent);

			string keyValue = "";
			bool valueSet = false;

			for (int j = 0; j < randomNumbers.Count; ++j)
			{
				if (randomNumbers[j] == tmpIndex)
				{
					if (Izanagi.Instance.SettingsType == SettingsMenu.Type.FAST_KANA)
						keyValue = answerString.ToString();
					else
						keyValue = answerString[j].ToString();

					valueSet = true;
				}
			}

			if (!valueSet)
			{
				if (Izanagi.Instance.SettingsType == SettingsMenu.Type.FAST_KANA)
					keyValue = Izanagi.Instance.MyDictionary.GetRandomAnswerCharacterKanas(currentQuestionType, currentAnswer.FirstKana);
				else
					keyValue = Izanagi.Instance.MyDictionary.GetRandomAnswerCharacterVocabulary(currentQuestionType);
			}

			entry.GetComponent<Button>().onClick.AddListener(() => EnterCharacter(keyValue));
			entry.GetComponentInChildren<Text>().text = keyValue;

			keyboardKeys.Add(entry);
		}
	}

	private void EnterCharacter(string value)
	{
		keyboardAnswerText.text += value;
	}

	private void FillMultipleChoice(int choiceCount)
	{
		int random = Random.Range(0, choiceCount);

		for (int i = 0; i < choiceCount; ++i)
		{
			if (i == random)
				multipleChoiceButtonsText[i].text = Izanagi.GetAnswerString(currentQuestionType, currentAnswer);
			else
			{
				if (Izanagi.Instance.SettingsType == SettingsMenu.Type.FAST_KANA)
					multipleChoiceButtonsText[i].text = Izanagi.Instance.MyDictionary.GetRandomAnswerCharacterKanas(currentQuestionType, currentAnswer.FirstKana);
				else
					multipleChoiceButtonsText[i].text = Izanagi.Instance.MyDictionary.GetRandomAnswerString(currentQuestionType, currentAnswer);
			}
		};
	}

	public void ValidateAnswer(Text answer)
	{
		ValidateAnswerPrivate(answer.text);
	}

	private void ValidateAnswerPrivate(string answer)
	{
		if (transitionToNextQuestion)
			return;

		bool hasWon = false;
		string answerFormat = Izanagi.GetAnswerString(currentQuestionType, currentAnswer);
		if (answer != string.Empty && answer == answerFormat)
		{
			hasWon = true;
		}
		else
		{
			List<string> answers = Izanagi.GetAnswers(currentQuestionType, currentAnswer);
			for (int i = 0; i < answers.Count; ++i)
			{
				if (answer != string.Empty && answers[i] == answer)
					hasWon = true;
			}
		}

		switch(currentAnswerType)
		{
			case AnswerType.DUAL_CHOICES:
				MultipleChoiceShowAnswer(hasWon, answer, 2);
			break;
			case AnswerType.QUARTET_CHOICES:
				MultipleChoiceShowAnswer(hasWon, answer, 4);
				break;
			case AnswerType.KEYBOARD:
				KeyboardShowAnswer(hasWon);
			break;
			default:
				break;
		}

		++currentQuestionCount;

		if (Izanagi.Instance.SettingsType == SettingsMenu.Type.TRAINING || Izanagi.Instance.SettingsType == SettingsMenu.Type.FAST_KANA)
			UpdateStats(hasWon, gameTimer.GetQuestionTime(), ref currentAnswer.ExercicesStats);
		else if (Izanagi.Instance.SettingsType == SettingsMenu.Type.TEST)
			UpdateStats(hasWon, gameTimer.GetQuestionTime(), ref currentAnswer.TestsStats);

		// SAVE STATS IN DATABASE
		if (Izanagi.Instance.SettingsType == SettingsMenu.Type.TRAINING || Izanagi.Instance.SettingsType == SettingsMenu.Type.TEST)
			Izanagi.Instance.MyDictionary.UpdateVocabularyDatabaseWithData(currentAnswer);
		else if (Izanagi.Instance.SettingsType == SettingsMenu.Type.FAST_KANA)
			Izanagi.Instance.MyDictionary.UpdateKanasDatabaseWithData(currentAnswer);

		Izanagi.Instance.GameSettings.AllRecapDatas.Add(new RecapData(currentAnswer, questionText.text, answerString, answer, hasWon));

		if (Izanagi.Instance.GameSettings.QuestionCount > 0 && currentQuestionCount > Izanagi.Instance.GameSettings.QuestionCount)
			GameOver();
		else if (!gameIsOver)
			NextQuestion();
	}

	private void UpdateStats(bool HasWon, float time, ref Datas.Stats stats)
	{
		stats.AverageTime = ((stats.AverageTime * stats.Total) + time) / (stats.Total + 1);
		stats.Total += 1;

		if (HasWon)
			stats.Good += 1;
		else
			stats.Wrong += 1;

		stats.Percent = ((float)stats.Good / (float)stats.Total) * 100f;
	}

	public void MultipleChoiceShowAnswer(bool hasWon, string playerAnswer, int multipleChoiceCount)
	{
		for (int i = 0; i < multipleChoiceCount; ++i)
		{
			Text choiceText = multipleChoiceButtonsText[i];

			if (playerAnswer != string.Empty && playerAnswer == choiceText.text)
			{
				if (hasWon)
					choiceText.transform.parent.GetComponent<Image>().color = new Color(113f / 255f, 1f, 0f);
				else
					choiceText.transform.parent.GetComponent<Image>().color = new Color(1f, 65f/255f, 0f);
			}
			else if (choiceText.text == answerString)
			{
				choiceText.transform.parent.GetComponent<Image>().color = new Color(113f / 255f, 1f, 0f);
			}
		}
	}

	public void KeyboardShowAnswer(bool hasWon)
	{
		if (hasWon)
		{
			keyboardAnswerText.color = new Color(113f / 255f, 1f, 0f);
		}
		else
		{
			keyboardAnswerText.color = new Color(1f, 65f / 255f, 0f);

			keyboardAnswerText.text = Izanagi.GetAnswerString(currentQuestionType, currentAnswer);
			keyboardAnswerText.text = keyboardAnswerText.text.Replace("/", " or ");
			answerString = keyboardAnswerText.text;
		}
	}

	public void CancelAnswer()
	{
		if (transitionToNextQuestion)
			return;

		keyboardAnswerText.text = "";
	}

	private void TimerDone()
	{
		if (Izanagi.Instance.SettingsType != SettingsMenu.Type.TEST)
			GameOver();
		else
			ValidateAnswerPrivate(keyboardAnswerText.text);
	}

	private void NextQuestion()
	{
		gameTimer.SetTimerState(true);
		transitionToNextQuestion = true;

		StartCoroutine(NextQuestionRoutine());
	}

	private IEnumerator NextQuestionRoutine()
	{
		yield return new WaitForSeconds(0.5f);

		if (Izanagi.Instance.SettingsType == SettingsMenu.Type.TEST)
			gameTimer.Reset();
		else
			gameTimer.SetTimerState(false);

		currentAnswer = AllDatasUsed[Random.Range(0, AllDatasUsed.Count)];
		RandomizeTypes();
		answerString = Izanagi.GetRealAnswerString(currentQuestionType, currentAnswer);
		SetupAnswerType();
		SetupQuestion();

		keyboardAnswerText.text = "";
		keyboardAnswerText.color = new Color(50f / 255f, 50f / 255f, 50f / 255f);

		for (int i = 0; i < multipleChoiceButtonsText.Count; ++i)
			multipleChoiceButtonsText[i].transform.parent.GetComponent<Image>().color = Color.white;

		transitionToNextQuestion = false;
	}

	private void GameOver()
	{
		gameIsOver = true;
		Izanagi.Instance.ChangeMenu("recapmenu");
	}

	public override void Close()
	{
		base.Close();
	}

	/*****************************************/
	/****************PAUSE MENU***************/
	/*****************************************/

	public void OpenPauseMenu()
	{
		if (gameIsOver)
			return;

		pauseMenu.SetActive(true);
		gameTimer.SetTimerState(true);
	}

	public void ResumePauseMenu()
	{
		pauseMenu.SetActive(false);
		gameTimer.SetTimerState(true);
	}

	public void Quit()
	{
		pauseMenu.SetActive(false);
		GameOver();
	}
}
