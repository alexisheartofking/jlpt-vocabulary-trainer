﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : Menu
{
	public override void Initialize()
	{
		base.Initialize();
	}

	public override void Open()
	{
		base.Open();
	}

	public override void Close()
	{
		base.Close();
	}

	public void GoToTrainingSettings()
	{
		Izanagi.Instance.SettingsType = SettingsMenu.Type.TRAINING;
		Izanagi.Instance.ChangeMenu("settingsmenu");
	}

	public void GoToTestSettings()
	{
		Izanagi.Instance.SettingsType = SettingsMenu.Type.TEST;
		Izanagi.Instance.ChangeMenu("settingsmenu");
	}

	public void GoToFastKanaSettings()
	{
		Izanagi.Instance.SettingsType = SettingsMenu.Type.FAST_KANA;
		Izanagi.Instance.ChangeMenu("settingsmenu");
	}

	public void ExitApp()
	{
		Application.Quit();
	}
}
