﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecapMenu : Menu
{
	[Header("Entries")]
	[SerializeField]
	private Transform entryPrefab;
	[SerializeField]
	private Transform listParent;

	[Header("Infos")]
	[SerializeField]
	private Text pageText;

	[Header("Buttons")]
	[SerializeField]
	private Button mainMenuButton;
	[SerializeField]
	private Button firstPageButton;
	[SerializeField]
	private Button previousPageButton;
	[SerializeField]
	private Button nextPageButton;
	[SerializeField]
	private Button LastPageButton;

	private List<Transform> entries;

	private int currentPage = 1;
	private int entryPerPage = 15;
	private int totalPage = 1;

	private List<RecapData> recapDatas;

	public override void Initialize()
	{
		base.Initialize();

		mainMenuButton.onClick.AddListener(GoToMainMenu);
		firstPageButton.onClick.AddListener(FirstPage);
		previousPageButton.onClick.AddListener(PreviousPage);
		nextPageButton.onClick.AddListener(NextPage);
		LastPageButton.onClick.AddListener(LastPage);

		entries = new List<Transform>();
	}

	public override void Open()
	{
		base.Open();

		recapDatas = Izanagi.Instance.GameSettings.AllRecapDatas;

		Refresh();
	}

	public void Refresh()
	{
		currentPage = 1;

		totalPage = (recapDatas.Count / entryPerPage);

		if (recapDatas.Count % entryPerPage != 0)
			++totalPage;

		NextPage(currentPage);
	}

	public override void Close()
	{
		base.Close();
	}

	private void GoToMainMenu()
	{
		Izanagi.Instance.ChangeMenu("MainMenu");
	}

	private void NextPage()
	{
		NextPage(currentPage + 1);
	}

	private void PreviousPage()
	{
		NextPage(currentPage - 1);
	}

	private void FirstPage()
	{
		NextPage(1);
	}

	private void LastPage()
	{
		NextPage(totalPage);
	}

	public void NextPage(int NextPageIndex)
	{
		currentPage = NextPageIndex;

		if (currentPage == 1)
		{
			firstPageButton.interactable = false;
			previousPageButton.interactable = false;
		}
		else
		{
			firstPageButton.interactable = true;
			previousPageButton.interactable = true;
		}

		if (currentPage == totalPage)
		{
			nextPageButton.interactable = false;
			LastPageButton.interactable = false;
		}
		else
		{
			nextPageButton.interactable = true;
			LastPageButton.interactable = true;
		}

		CreateEntries();
		UpdatePageText();
	}

	private void ClearEntries()
	{
		foreach (Transform child in listParent)
		{
			Destroy(child.gameObject);
		}
	}

	private List<RecapData> GetRange(List<RecapData> AllDatas)
	{
		if (AllDatas.Count > 0)
		{
			if (currentPage == totalPage)
				return AllDatas.GetRange((currentPage - 1) * entryPerPage, AllDatas.Count - ((currentPage - 1) * entryPerPage));
			else
				return AllDatas.GetRange((currentPage - 1) * entryPerPage, entryPerPage);
		}

		return new List<RecapData>();
	}

	private void CreateEntries()
	{
		ClearEntries();

		List<RecapData> dataRanged = GetRange(recapDatas);

		for (int i = 0; i < dataRanged.Count; ++i)
		{
			int tmpIndex = i;
			Transform entry = GameObject.Instantiate(entryPrefab, listParent);
			entry.GetComponent<RecapEntry>().Initialize(dataRanged[i]);
			entries.Add(entry);
		}
	}

	private void UpdatePageText()
	{
		pageText.text = "Page " + currentPage.ToString() + "/" + Mathf.Max(totalPage, 1).ToString();
	}
}