﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : Menu
{
	public enum Type
	{
		TRAINING,
		TEST,
		FAST_KANA
	};

	[SerializeField]
	private Text titleText;

	[SerializeField]
	private SingleFilterWithCustom wordCountSetting;
	[SerializeField]
	private SingleFilterWithCustom questionCountSetting;
	[SerializeField]
	private SingleFilterWithCustom questionTotalTimeSetting;
	[SerializeField]
	private SingleFilterWithCustom timerPerQuestionSetting;
	[SerializeField]
	private LearningVocabularyFiltersHandler.StatsFilter testStatsSettings;
	[SerializeField]
	private LearningVocabularyFiltersHandler.StatsFilter trainingStatsSettings;
	[SerializeField]
	private SingleFilterWithCustom kanaTypeSettings;
	[SerializeField]
	private JLPTSettings JLPTSettings;
	[SerializeField]
	private List<QuestionTypeSettings> questionTypeSettings;
	[SerializeField]
	private AnswerTypeSettings answerTypeSettings;
	[SerializeField]
	private List<FirstKanaSettings> firstKanaSettings;
	[SerializeField]
	private List<FirstKanaSettings> listKanaSettings;

	private Type currentType = Type.TRAINING;

	private List<string> errorList;

	public override void Initialize()
	{
		base.Initialize();

		errorList = new List<string>();
	}

	public override void Open()
	{
		base.Open();

		ResetAllFilters();

		currentType = Izanagi.Instance.SettingsType;

		if (currentType == Type.TRAINING)
		{
			titleText.text = "Training";

			questionTotalTimeSetting.gameObject.SetActive(true);
			timerPerQuestionSetting.gameObject.SetActive(false);

			JLPTSettings.gameObject.SetActive(true);
			kanaTypeSettings.gameObject.SetActive(false);

			questionTypeSettings[0].transform.parent.parent.gameObject.SetActive(true);
			testStatsSettings.parent.SetActive(true);
			trainingStatsSettings.parent.SetActive(false);

			firstKanaSettings[0].transform.parent.parent.gameObject.SetActive(true);
			listKanaSettings[0].transform.parent.parent.gameObject.SetActive(false);
		}
		else if (currentType == Type.TEST)
		{
			titleText.text = "Test";

			questionTotalTimeSetting.gameObject.SetActive(false);
			timerPerQuestionSetting.gameObject.SetActive(true);

			JLPTSettings.gameObject.SetActive(true);
			kanaTypeSettings.gameObject.SetActive(false);

			questionTypeSettings[0].transform.parent.parent.gameObject.SetActive(true);
			testStatsSettings.parent.SetActive(true);
			trainingStatsSettings.parent.SetActive(false);

			firstKanaSettings[0].transform.parent.parent.gameObject.SetActive(true);
			listKanaSettings[0].transform.parent.parent.gameObject.SetActive(false);
		}
		else if (currentType == Type.FAST_KANA)
		{
			titleText.text = "Fast Kana";

			questionTotalTimeSetting.gameObject.SetActive(true);
			timerPerQuestionSetting.gameObject.SetActive(false);

			JLPTSettings.gameObject.SetActive(false);
			kanaTypeSettings.gameObject.SetActive(true);

			questionTypeSettings[0].transform.parent.parent.gameObject.SetActive(false);
			testStatsSettings.parent.SetActive(false);
			trainingStatsSettings.parent.SetActive(true);

			firstKanaSettings[0].transform.parent.parent.gameObject.SetActive(false);
			listKanaSettings[0].transform.parent.parent.gameObject.SetActive(true);
		}
	}

	private void ResetAllFilters()
	{
		wordCountSetting.Reset();
		questionCountSetting.Reset();
		questionTotalTimeSetting.Reset();
		timerPerQuestionSetting.Reset();
		testStatsSettings.Reset();
		trainingStatsSettings.Reset();
		kanaTypeSettings.Reset();
		JLPTSettings.Reset();

		for (int i = 0; i < questionTypeSettings.Count; ++i)
			questionTypeSettings[i].Reset();

		answerTypeSettings.Reset();

		for (int i = 0; i < firstKanaSettings.Count; ++i)
			firstKanaSettings[i].Reset();

		for (int i = 0; i < listKanaSettings.Count; ++i)
			listKanaSettings[i].Reset();
	}

	public void StartTraining()
	{
		errorList = new List<string>();

		if (currentType == Type.TEST || currentType == Type.TRAINING)
		{
			if (!Izanagi.Instance.GameSettings.IsCompatibleQuestionsAndAnswers())
				errorList.Add("We can't make 'X => English' questions type with 'Keyboard' answers");
			else if (!Izanagi.Instance.GameSettings.IsCompatibleQuestionsAndAnswer())
				errorList.Add("We can't do questions with 'X => English' and only 'Keyboard' as answer");

			List<Datas> tmpAllDatas = new List<Datas>();

			for (int i = 0; i < Izanagi.Instance.GameSettings.JLPTLevels.Count; ++i)
				tmpAllDatas.AddRange(Izanagi.Instance.MyDictionary.Get((int)Izanagi.Instance.GameSettings.JLPTLevels[i]));

			if (tmpAllDatas.Count == 0)
				errorList.Add("We couldn't found any question with these JLPT levels");

			if (Izanagi.Instance.GameSettings.FirstKanas.Count <= 0)
				errorList.Add("You haven't chosen any first kana");
			else
			{
				tmpAllDatas = tmpAllDatas.FindAll(
					delegate (Datas data)
					{
						Debug.Log(ApplyFirstKanaFilter(data).ToString() + " / " + ApplyTrainingFilter(data.TestsStats, testStatsSettings).ToString() + " / " + CompatibleAnswer(data));

						if (ApplyFirstKanaFilter(data)
						&& ApplyTrainingFilter(data.TestsStats, testStatsSettings)
						&& CompatibleAnswer(data))
							return true;

						return false;
					}
				);

				if (tmpAllDatas.Count == 0)
					errorList.Add("We couldn't found any question with these first kanas or tests stats");
				else
				{
					int wordCount = 0;
					bool canParse = int.TryParse(wordCountSetting.CurrentValue.text, out wordCount);
					if (!canParse)
					{
						Izanagi.Instance.GameSettings.AllDatas = tmpAllDatas;
					}
					else
					{
						if (wordCount <= 0)
							errorList.Add("The word count can't be below or equal zero");
						else
						{
							List<int> randomNumbers = new List<int>();
							do
							{
								int random = Random.Range(0, tmpAllDatas.Count);

								if (!randomNumbers.Contains(random))
									randomNumbers.Add(random);
							}
							while (randomNumbers.Count < wordCount);

							for (int i = 0; i < randomNumbers.Count; ++i)
								Izanagi.Instance.GameSettings.AllDatas.Add(tmpAllDatas[randomNumbers[i]]);
						}
					}
				}
			}
		}
		else if (currentType == Type.FAST_KANA)
		{
			if (Izanagi.Instance.GameSettings.FirstKanas.Count <= 0)
				errorList.Add("You haven't chosen any first kana");
			else
			{
				Izanagi.Instance.GameSettings.AllDatas = new List<Datas>();

				int start = 0, end = 0;

				switch(kanaTypeSettings.CurrentValue.text)
				{
					case "ひらがな":
						Izanagi.Instance.GameSettings.KanaType = KanaType.HIRAGANA;
						start = 0;
						end = 46;
						break;
					case "カタカナ":
						Izanagi.Instance.GameSettings.KanaType = KanaType.KATAKANA;
						start = 46;
						end = 92;
						break;
					case "Both":
						start = 0;
						end = 92;
						Izanagi.Instance.GameSettings.KanaType = KanaType.BOTH;
						break;
				}

				List<Datas> tmpAllDatas = new List<Datas>();
				for (int i = start; i < end; ++i)
				{
					Datas data = Izanagi.Instance.MyDictionary.Kanas[i];

					if (ApplyFirstKanaFilter(data)
						&& ApplyTrainingFilter(data.ExercicesStats, trainingStatsSettings))
						tmpAllDatas.Add(data);
				}

				if (tmpAllDatas.Count == 0)
					errorList.Add("We couldn't found any question with these first kanas or tests stats");
				else
				{
					int wordCount = 0;
					bool canParse = int.TryParse(wordCountSetting.CurrentValue.text, out wordCount);
					if (!canParse)
					{
						Izanagi.Instance.GameSettings.AllDatas = tmpAllDatas;
					}
					else
					{
						if (wordCount <= 0)
							errorList.Add("The word count can't be below or equal zero");
						else
						{
							List<int> randomNumbers = new List<int>();
							do
							{
								int random = Random.Range(0, tmpAllDatas.Count);

								if (!randomNumbers.Contains(random))
									randomNumbers.Add(random);
							}
							while (randomNumbers.Count < wordCount);

							for (int i = 0; i < randomNumbers.Count; ++i)
								Izanagi.Instance.GameSettings.AllDatas.Add(tmpAllDatas[randomNumbers[i]]);
						}
					}
				}
			}

			Izanagi.Instance.GameSettings.QuestionTypes.Add(QuestionType.KANA_TO_ROMAJI);
			Izanagi.Instance.GameSettings.QuestionTypes.Add(QuestionType.ROMAJI_TO_KANA);
		}

		bool couldParse = int.TryParse(questionCountSetting.CurrentValue.text, out Izanagi.Instance.GameSettings.QuestionCount);

		if (!couldParse)
			Izanagi.Instance.GameSettings.QuestionCount = -1;
		else
		{
			if (Izanagi.Instance.GameSettings.QuestionCount <= 0)
				errorList.Add("The questions count can't be below or equal zero");
		}

		if (currentType == Type.TEST)
		{
			couldParse = int.TryParse(timerPerQuestionSetting.CurrentValue.text, out Izanagi.Instance.GameSettings.TotalTimer);

			if (!couldParse)
			{
				couldParse = int.TryParse(timerPerQuestionSetting.CurrentValue.text.Remove(timerPerQuestionSetting.CurrentValue.text.Length - 1), out Izanagi.Instance.GameSettings.TotalTimer);

				if (!couldParse)
					Izanagi.Instance.GameSettings.UseTimer = false;
				else
				{
					Izanagi.Instance.GameSettings.UseTimer = true;

					if (Izanagi.Instance.GameSettings.TotalTimer <= 0)
						errorList.Add("The time per question can't be below or equal zero");
				}
			}
			else
			{
				Izanagi.Instance.GameSettings.UseTimer = true;

				if (Izanagi.Instance.GameSettings.TotalTimer <= 0)
					errorList.Add("The timer per question can't be below or equal zero");
			}

		}
		else if (currentType == Type.TRAINING || currentType == Type.FAST_KANA)
		{
			Debug.Log(questionTotalTimeSetting.CurrentValue.text);
			couldParse = int.TryParse(questionTotalTimeSetting.CurrentValue.text, out Izanagi.Instance.GameSettings.TotalTimer);

			if (!couldParse)
			{
				Debug.Log(questionTotalTimeSetting.CurrentValue.text.Remove(questionTotalTimeSetting.CurrentValue.text.Length - 1));
				couldParse = int.TryParse(questionTotalTimeSetting.CurrentValue.text.Remove(questionTotalTimeSetting.CurrentValue.text.Length - 1), out Izanagi.Instance.GameSettings.TotalTimer);

				if (!couldParse)
					Izanagi.Instance.GameSettings.UseTimer = false;
				else
				{
					Izanagi.Instance.GameSettings.UseTimer = true;

					if (Izanagi.Instance.GameSettings.TotalTimer <= 0)
						errorList.Add("The questions total time can't be below or equal zero");
				}
			}
			else
			{
				Izanagi.Instance.GameSettings.UseTimer = true;

				if (Izanagi.Instance.GameSettings.TotalTimer <= 0)
					errorList.Add("The questions total time can't below or equal zero");
			}
		}

		if (Izanagi.Instance.GameSettings.QuestionTypes.Count <= 0)
			errorList.Add("You haven't chosen any question type");

		if (Izanagi.Instance.GameSettings.AnswerTypes.Count <= 0)
			errorList.Add("You haven't chosen any answer type");

		if (errorList.Count > 0)
			ErrorFound();
		else
			Izanagi.Instance.ChangeMenu("gamemenu");
	}

	private bool CompatibleAnswer(Datas data)
	{
		for (int i = 0; i < Izanagi.Instance.GameSettings.QuestionTypes.Count; ++i)
		{
			if (Izanagi.GetQuestionString(Izanagi.Instance.GameSettings.QuestionTypes[i], data) == string.Empty)
				return false;
			else if (Izanagi.GetAnswerString(Izanagi.Instance.GameSettings.QuestionTypes[i], data) == string.Empty)
				return false;
		}

		return true;
	}

	private void ErrorFound()
	{
		string error = "";

		if (currentType == Type.TRAINING)
			error = "Exercices couldn't be started because:\n\n";
		else if (currentType == Type.TEST)
			error = "The tests couldn't be started because:\n\n";

		for (int i = 0; i < errorList.Count; ++i)
		{
			error += "- " + errorList[i];

			if (i < errorList.Count - 1)
				error += "\n";
		}

		Izanagi.Instance.ErrorPanelHandler.Open(error);
	}

	private bool ApplyFirstKanaFilter(Datas data)
	{
		for (int i = 0; i < Izanagi.Instance.GameSettings.FirstKanas.Count; ++i)
		{
			if (Izanagi.Instance.GameSettings.FirstKanas[i] == data.FirstKana)
				return true;
		}

		return false;
	}

	private bool ApplyTrainingFilter(Datas.Stats stats, LearningVocabularyFiltersHandler.StatsFilter statsFilter)
	{
		if (int.Parse(statsFilter.resultsMinFilter.text) <= stats.Percent && stats.Percent <= float.Parse(statsFilter.resultsMaxFilter.text))
		{
			if (statsFilter.averageTimeOperatorFilter.value == 0 && stats.AverageTime >= float.Parse(statsFilter.averageTimeFilter.text))
				return true;
			else if (statsFilter.averageTimeOperatorFilter.value == 1 && stats.AverageTime > float.Parse(statsFilter.averageTimeFilter.text))
				return true;
			else if (statsFilter.averageTimeOperatorFilter.value == 2 && stats.AverageTime <= float.Parse(statsFilter.averageTimeFilter.text))
				return true;
			else if (statsFilter.averageTimeOperatorFilter.value == 3 && stats.AverageTime < float.Parse(statsFilter.averageTimeFilter.text))
				return true;
			else if (statsFilter.averageTimeOperatorFilter.value == 4 && stats.AverageTime == float.Parse(statsFilter.averageTimeFilter.text))
				return true;
		}

		return false;
	}

	public void AllSettings()
	{
		if (currentType == Type.TEST || currentType == Type.TRAINING)
		{
			JLPTSettings.SelectAll();
			AllQuestionType();
			AllKanas();
		}
		else if (currentType == Type.FAST_KANA)
		{
			AllListKanas();
		}

		answerTypeSettings.SelectAll();
	}

	public void NoneSettings()
	{
		if (currentType == Type.TEST || currentType == Type.TRAINING)
		{
			JLPTSettings.UnselectAll();
			NoneQuestionType();
			NoneKanas();
		}
		else if (currentType == Type.FAST_KANA)
		{
			NoneListKanas();
		}

		answerTypeSettings.UnselectAll();
	}

	public void AllKanas()
	{
		for (int i = 0; i < firstKanaSettings.Count; ++i)
			firstKanaSettings[i].SelectAll();
	}

	public void NoneKanas()
	{
		for (int i = 0; i < firstKanaSettings.Count; ++i)
			firstKanaSettings[i].UnselectAll();
	}

	public void AllListKanas()
	{
		for (int i = 0; i < firstKanaSettings.Count; ++i)
			listKanaSettings[i].SelectAll();
	}

	public void NoneListKanas()
	{
		for (int i = 0; i < firstKanaSettings.Count; ++i)
			listKanaSettings[i].UnselectAll();
	}

	public void AllQuestionType()
	{
		for (int i = 0; i < questionTypeSettings.Count; ++i)
			questionTypeSettings[i].SelectAll();
	}

	public void NoneQuestionType()
	{
		for (int i = 0; i < questionTypeSettings.Count; ++i)
			questionTypeSettings[i].UnselectAll();
	}
}
