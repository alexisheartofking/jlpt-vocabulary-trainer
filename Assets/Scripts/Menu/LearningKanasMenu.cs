﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LearningKanasMenu : Menu
{
	[Header("Entries")]
	[SerializeField]
	private Transform entryPrefab;
	[SerializeField]
	private Transform listParent;

	[Header("Infos")]
	[SerializeField]
	private Text pageText;

	[Header("Buttons")]
	[SerializeField]
	private Button firstPageButton;
	[SerializeField]
	private Button previousPageButton;
	[SerializeField]
	private Button nextPageButton;
	[SerializeField]
	private Button LastPageButton;

	private List<Transform> entries;

	[Header("Filters")]
	[SerializeField]
	private LearninKanasFiltersHandler filtersHandler;

	private int currentPage = 1;
	private int entryPerPage = 15;
	private int totalPage = 1;

	private List<Datas> datasOrdered;

	public override void Initialize()
	{
		base.Initialize();

		firstPageButton.onClick.AddListener(FirstPage);
		previousPageButton.onClick.AddListener(PreviousPage);
		nextPageButton.onClick.AddListener(NextPage);
		LastPageButton.onClick.AddListener(LastPage);

		entries = new List<Transform>();
	}

	public override void Open()
	{
		base.Open();

		Refresh();
	}

	public override void Close()
	{
		base.Close();
	}

	public void Refresh()
	{
		currentPage = 1;
		Order();

		totalPage = (datasOrdered.Count / entryPerPage);

		if (datasOrdered.Count % entryPerPage != 0)
			++totalPage;

		NextPage(currentPage);
	}

	private void Order()
	{
		datasOrdered = Izanagi.Instance.MyDictionary.Kanas;

		int start = 0, end = 0;
		switch (filtersHandler.KanaFilter.captionText.text)
		{
			case "Hiragana":
				start = 0;
				end = 46;
				break;
			case "Katakana":
				start = 46;
				end = 92;
				break;
			case "Both":
				start = 0;
				end = 92;
				break;
		}

		datasOrdered = datasOrdered.GetRange(start, end - start);
		datasOrdered = datasOrdered.FindAll(delegate (Datas data)
		{
			if (ApplyFirstKanaFilter(data)
			&& ApplyTrainingFilter(data.ExercicesStats, filtersHandler.exercicesFilter)
			&& ApplySearchFilter(data))
				return true;

			return false;
		});
	}

	private bool ApplyFirstKanaFilter(Datas data)
	{
		if (filtersHandler.FirstKanaFilter.value == 0 || data.FirstKana == filtersHandler.FirstKanaFilter.captionText.text)
			return true;

		return false;
	}

	private bool ApplyTrainingFilter(Datas.Stats stats, LearningVocabularyFiltersHandler.StatsFilter statsFilter)
	{
		if (int.Parse(statsFilter.resultsMinFilter.text) <= stats.Percent && stats.Percent <= float.Parse(statsFilter.resultsMaxFilter.text))
		{
			if (statsFilter.averageTimeOperatorFilter.value == 0 && stats.AverageTime >= float.Parse(statsFilter.averageTimeFilter.text))
				return true;
			else if (statsFilter.averageTimeOperatorFilter.value == 1 && stats.AverageTime > float.Parse(statsFilter.averageTimeFilter.text))
				return true;
			else if (statsFilter.averageTimeOperatorFilter.value == 2 && stats.AverageTime <= float.Parse(statsFilter.averageTimeFilter.text))
				return true;
			else if (statsFilter.averageTimeOperatorFilter.value == 3 && stats.AverageTime < float.Parse(statsFilter.averageTimeFilter.text))
				return true;
			else if (statsFilter.averageTimeOperatorFilter.value == 4 && stats.AverageTime == float.Parse(statsFilter.averageTimeFilter.text))
				return true;
		}

		return false;
	}

	private bool ApplySearchFilter(Datas data)
	{
		if (filtersHandler.searchFilter.text.Length == 0)
			return true;
		else
		{
			string[] filters = filtersHandler.searchFilter.text.Trim().Split(',');

			for (int i = 0; i < filters.Length; ++i)
			{
				if (data.Kana.Contains(filters[i])
					|| data.Romaji.Contains(filters[i]))
					return true;
			}
		}

		return false;
	}

	private void CreateEntries()
	{
		ClearEntries();

		List<Datas> dataRanged = GetRange(datasOrdered);

		for (int i = 0; i < dataRanged.Count; ++i)
		{
			int tmpIndex = i;
			Transform entry = GameObject.Instantiate(entryPrefab, listParent);
			entry.GetComponent<LearningKanaEntry>().Initialize(dataRanged[i]);
			entries.Add(entry);
		}
	}

	private List<Datas> GetRange(List<Datas> AllDatas)
	{
		if (AllDatas.Count > 0)
		{
			if (currentPage == totalPage)
				return AllDatas.GetRange((currentPage - 1) * entryPerPage, AllDatas.Count - ((currentPage - 1) * entryPerPage));
			else
				return AllDatas.GetRange((currentPage - 1) * entryPerPage, entryPerPage);
		}

		return new List<Datas>();
	}

	private void ClearEntries()
	{
		foreach (Transform child in listParent)
		{
			Destroy(child.gameObject);
		}
	}

	private void UpdatePageText()
	{
		pageText.text = "Page " + currentPage.ToString() + "/" + Mathf.Max(totalPage, 1).ToString();
	}

	private void NextPage()
	{
		NextPage(currentPage + 1);
	}

	private void PreviousPage()
	{
		NextPage(currentPage - 1);
	}

	private void FirstPage()
	{
		NextPage(1);
	}

	private void LastPage()
	{
		NextPage(totalPage);
	}

	public void NextPage(int NextPageIndex)
	{
		currentPage = NextPageIndex;

		if (currentPage == 1)
		{
			firstPageButton.interactable = false;
			previousPageButton.interactable = false;
		}
		else
		{
			firstPageButton.interactable = true;
			previousPageButton.interactable = true;
		}

		if (currentPage == totalPage)
		{
			nextPageButton.interactable = false;
			LastPageButton.interactable = false;
		}
		else
		{
			nextPageButton.interactable = true;
			LastPageButton.interactable = true;
		}

		CreateEntries();
		UpdatePageText();
	}
}
