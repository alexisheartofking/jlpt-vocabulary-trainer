﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LearningVocabularyFiltersHandler : MonoBehaviour
{
	[System.Serializable]
	public class StatsFilter
	{
		public GameObject parent;

		public InputField resultsMinFilter;
		public InputField resultsMaxFilter;
		public Dropdown averageTimeOperatorFilter;
		public InputField averageTimeFilter;

		public void Reset()
		{
			resultsMinFilter.SetTextWithoutNotify("0");
			resultsMaxFilter.SetTextWithoutNotify("100");
			averageTimeOperatorFilter.SetValueWithoutNotify(0);
			averageTimeFilter.SetTextWithoutNotify("0");
		}
	}

	[Header("JPLT")]
	public Dropdown JPLTFilter;

	[Header("First Kana")]
	public Dropdown FirstKanaFilter;

	[Header("Exercices")]
	public StatsFilter exercicesFilter;

	[Header("Tests")]
	public StatsFilter testsFilter;

	[Header("Search")]
	public InputField searchFilter;

	public void ResetFilters()
	{
		JPLTFilter.SetValueWithoutNotify(0);
		FirstKanaFilter.SetValueWithoutNotify(0);
		exercicesFilter.Reset();
		testsFilter.Reset();
		searchFilter.SetTextWithoutNotify("");
	}

	public void ResetSearchFilters()
	{
		searchFilter.SetTextWithoutNotify("");
	}
}
