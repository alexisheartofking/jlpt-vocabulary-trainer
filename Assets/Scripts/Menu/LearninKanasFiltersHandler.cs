﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LearninKanasFiltersHandler : MonoBehaviour
{
	[Header("Kana")]
	public Dropdown KanaFilter;

	[Header("First Kana")]
	public Dropdown FirstKanaFilter;

	[Header("Exercices")]
	public LearningVocabularyFiltersHandler.StatsFilter exercicesFilter;

	[Header("Search")]
	public InputField searchFilter;

	public void ResetFilters()
	{
		KanaFilter.SetValueWithoutNotify(0);
		FirstKanaFilter.SetValueWithoutNotify(0);
		exercicesFilter.Reset();
		searchFilter.SetTextWithoutNotify("");
	}

	public void ResetSearchFilters()
	{
		searchFilter.SetTextWithoutNotify("");
	}
}
