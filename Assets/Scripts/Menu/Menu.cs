﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 [System.Serializable]
public class Menu : MonoBehaviour
{
	private bool isInitialized = false;

	[Header("Parent Menu")]
	public CanvasGroup CanvasGroup;
	public AnimationCurve fadeIn;
	public AnimationCurve fadeOut;

	public virtual void Initialize()
	{

	}

	public virtual void Open()
	{
		if (!isInitialized)
		{
			Initialize();
			isInitialized = true;
		}
	}

	public virtual void LateOpen()
	{
	}

	public virtual void Close()
	{
	}

	public virtual void LateClose()
	{
	}
}
