﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorPanelHandler : MonoBehaviour
{
	[SerializeField]
	private Text errorText;

	public void Open(string ErrorText)
	{
		errorText.text = ErrorText;
		gameObject.SetActive(true);
	}

	public void Close()
	{
		gameObject.SetActive(false);
	}
}
