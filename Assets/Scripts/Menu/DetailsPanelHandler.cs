﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailsPanelHandler : MonoBehaviour
{
	[System.Serializable]
	public class Stats
	{
		[SerializeField]
		private GameObject Parent;
		[SerializeField]
		private Text ResultsText;
		[SerializeField]
		private Text AverageTimeText;

		public void Show()
		{
			Parent.SetActive(true);
		}

		public void Hide()
		{
			Parent.SetActive(false);
		}

		public void ComputeStats(Datas.Stats Stats)
		{
			ResultsText.text = Stats.Total.ToString() 
				+ " / <color=#00FF00>" + Stats.Good.ToString() + "</color>"
				+ " / <color=#FF0000>" + Stats.Wrong.ToString() + "</color> (";

			if (Stats.Percent <= 25)
				ResultsText.text += "<color=#FF0000>";
			else if (Stats.Percent <= 50)
				ResultsText.text += "<color=#FFA500>";
			else if (Stats.Percent <= 75)
				ResultsText.text += "<color=#FFFF00>";
			else
				ResultsText.text += "<color=#00FF00>";

			ResultsText.text += Stats.Percent.ToString("n2") + "%</color>)";

			AverageTimeText.text = Stats.AverageTime.ToString("n2") + "s";
		}
	}

	[Header("Vocabulary")]
	[SerializeField]
	private GameObject VocabularyParent;
	[SerializeField]
	private Text JLPTText;
	[SerializeField]
	private Text KanjiText;
	[SerializeField]
	private Text KanaText;
	[SerializeField]
	private Text RomajiText;
	[SerializeField]
	private Text MeaningText;

	[Header("Kanas")]
	[SerializeField]
	private GameObject KanasParent;
	[SerializeField]
	private Text Kana;
	[SerializeField]
	private Text Romaji;

	[SerializeField]
	private Stats ExercicesStats;
	[SerializeField]
	private Stats TestsStats;

	public void Open(Datas data, bool IsKana = false)
	{
		if (!IsKana)
		{
			VocabularyParent.SetActive(true);
			KanasParent.SetActive(false);

			TestsStats.Show();

			JLPTText.text = "JPLT:" + data.JLPT.ToString();
			KanjiText.text = Izanagi.FormatMultipleStringWithSlash(data.Kanji);
			KanaText.text = Izanagi.FormatMultipleStringWithSlash(data.Kana);
			RomajiText.text = Izanagi.FormatMultipleStringWithSlash(data.Romaji);
			MeaningText.text = data.GetCurrentTranslation();

			TestsStats.ComputeStats(data.TestsStats);
		}
		else
		{
			VocabularyParent.SetActive(false);
			KanasParent.SetActive(true);
			TestsStats.Hide();

			Kana.text = Izanagi.FormatMultipleStringWithSlash(data.Kana);
			Romaji.text = Izanagi.FormatMultipleStringWithSlash(data.Romaji);
		}

		ExercicesStats.ComputeStats(data.ExercicesStats);

		gameObject.SetActive(true);
	}

	public void Close()
	{
		gameObject.SetActive(false);
	}

}
