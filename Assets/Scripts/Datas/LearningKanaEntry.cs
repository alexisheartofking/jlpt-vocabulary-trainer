﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LearningKanaEntry : MonoBehaviour
{
	[SerializeField]
	private Text kanaText;
	[SerializeField]
	private Text romajiText;
	[SerializeField]
	private Text statsText;
	[SerializeField]
	private Text averageTimeText;

	[SerializeField]
	private Datas data;

	public void Initialize(Datas InData)
	{
		data = InData;

		kanaText.text = data.Kana[0];
		romajiText.text = data.Romaji[0];

		statsText.text = Izanagi.FormatMultipleStringBreakLine(InData.Kana);

		statsText.text = data.ExercicesStats.Total.ToString()
				+ " / <color=#00FF00>" + data.ExercicesStats.Good.ToString() + "</color>"
				+ " / <color=#FF0000>" + data.ExercicesStats.Wrong.ToString() + "</color> (";

		if (data.ExercicesStats.Percent <= 25)
			statsText.text += "<color=#FF0000>";
		else if (data.ExercicesStats.Percent <= 50)
			statsText.text += "<color=#FFA500>";
		else if (data.ExercicesStats.Percent <= 75)
			statsText.text += "<color=#FFFF00>";
		else
			statsText.text += "<color=#00FF00>";

		statsText.text += data.ExercicesStats.Percent.ToString("n2") + "%</color>)";

		averageTimeText.text = data.ExercicesStats.AverageTime.ToString("n2") + "s";

		GetComponent<Button>().onClick.AddListener(() => Izanagi.Instance.DetailsPanelHandler.Open(data, true));
	}
}
