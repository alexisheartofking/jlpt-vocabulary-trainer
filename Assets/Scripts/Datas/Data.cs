﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Datas
{
	[System.Serializable]
	public struct Stats
	{
		public int Total;
		public int Good;
		public int Wrong;
		public float Percent;
		public float AverageTime;

		public Stats(int Total, int Good, int Wrong, float Percent, float AverageTime)
		{
			this.Total = Total;
			this.Good = Good;
			this.Wrong = Wrong;
			this.Percent = Percent;
			this.AverageTime = AverageTime;
		}
	}

	public int JLPT;
	public string FirstKana;
	public List<string> Kanji;
	public List<string> Kana;
	public List<string> Romaji;
	public string English;
	public string French;
	public Stats ExercicesStats;
	public Stats TestsStats;

	public Datas(int InJLPT, string InFirstKana, string InKanji, string InKana, string InRomaji, string InEnglish, string InFrench, int ExercicesTotal, int ExercicesGood, int ExercicesWrong, float ExercicesPercent, float ExercicesAverageTime, int TestsTotal, int TestsGood, int TestsWrong, float TestsPercent, float TestsAverageTime)
	{
		JLPT = InJLPT;
		FirstKana = InFirstKana;

		FormalizeStringData(ref Kanji, InKanji);
		FormalizeStringData(ref Kana, InKana);
		FormalizeStringData(ref Romaji, InRomaji);

		English = InEnglish;
		French = InFrench;
		ExercicesStats = new Stats(ExercicesTotal, ExercicesGood, ExercicesWrong, ExercicesPercent, ExercicesAverageTime);
		TestsStats = new Stats(TestsTotal, TestsGood, TestsWrong, TestsPercent, TestsAverageTime);
	}

	private void FormalizeStringData(ref List<string> myList, string Datas)
	{
		myList = new List<string>();

		if (Datas == string.Empty)
			return;

		string[] finalDatas = Datas.Trim().Split('/');

		for (int i = 0; i < finalDatas.Length; i++)
			myList.Add(finalDatas[i]);
	}

	public string GetCurrentTranslation()
	{
		return French;
	}
}

[System.Serializable]
public class RecapData
{
	public Datas data;
	public string question;
	public string answer;
	public string yourAnswer;
	public bool hasWon;

	public RecapData(Datas inData, string inQuestion, string inAnswer, string inYourAnswer, bool inHasWon)
	{
		data = inData;
		question = inQuestion;
		answer = inAnswer;
		yourAnswer = inYourAnswer;
		hasWon = inHasWon;
	}
};