﻿using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class MyDictionary
{
	public MyDictionary()
	{
		SqliteHelper = new SqliteHelper();
		GatherAllDatas();
	}

	private Dictionary<int, List<Datas>> AllDatas;
	public List<Datas> Kanas;
	private SqliteHelper SqliteHelper;

	private void GatherAllDatas()
	{
		AllDatas = new Dictionary<int, List<Datas>>();
		AllDatas.Add(1, new List<Datas>());
		AllDatas.Add(2, new List<Datas>());
		AllDatas.Add(3, new List<Datas>());
		AllDatas.Add(4, new List<Datas>());
		AllDatas.Add(5, new List<Datas>());

		IDataReader dataReader = SqliteHelper.GetAllData("Vocabulary");
		while (dataReader.Read())
		{
			AllDatas[int.Parse(dataReader[0].ToString())].Add(new Datas(
				int.Parse(dataReader[0].ToString()),
				dataReader[1].ToString(),
				dataReader[2].ToString(),
				dataReader[3].ToString(),
				dataReader[4].ToString(),
				dataReader[5].ToString(),
				dataReader[6].ToString(),
				int.Parse(dataReader[7].ToString()),
				int.Parse(dataReader[8].ToString()),
				int.Parse(dataReader[9].ToString()),
				float.Parse(dataReader[10].ToString()),
				float.Parse(dataReader[11].ToString()),
				int.Parse(dataReader[12].ToString()),
				int.Parse(dataReader[13].ToString()),
				int.Parse(dataReader[14].ToString()),
				float.Parse(dataReader[15].ToString()),
				float.Parse(dataReader[16].ToString())
			));
		}

		Kanas = new List<Datas>();
		dataReader = SqliteHelper.GetAllData("Kanas");
		while (dataReader.Read())
		{
			Kanas.Add(new Datas(
				-1,
				dataReader[0].ToString(), 
				"", 
				dataReader[1].ToString(), 
				dataReader[2].ToString(), 
				"", 
				"",
				int.Parse(dataReader[3].ToString()),
				int.Parse(dataReader[4].ToString()),
				int.Parse(dataReader[5].ToString()),
				float.Parse(dataReader[6].ToString()),
				float.Parse(dataReader[7].ToString()),
				0, 
				0, 
				0,
				0, 
				0
			));
		}
	}

	public List<Datas> Get(int JLPT)
	{
		if (JLPT != 0)
			return AllDatas[JLPT];
		else
		{
			List<Datas> allList = new List<Datas>();

			for (int i = 1; i < 6; ++i)
				allList.AddRange(AllDatas[i]);

			return allList;
		}
	}

	public string GetRandomAnswerString(QuestionType questionType, Datas currentAnswer)
	{
		List<Datas> tmpList;
		do
		{
			tmpList = AllDatas[Random.Range(1, 6)];
		}
		while (tmpList.Count == 0);

		string answerString = "";
		string currentAnswerString = Izanagi.GetAnswerString(questionType, currentAnswer);
		do
		{
			answerString = Izanagi.GetAnswerString(questionType, tmpList[Random.Range(0, tmpList.Count)]);
		}
		while (answerString != string.Empty && answerString == currentAnswerString);

		return answerString;
	}

	public string GetRandomAnswerCharacterVocabulary(QuestionType questionType)
	{
		List<Datas> tmpList;
		do
		{
			tmpList = AllDatas[Random.Range(1, 6)];
		}
		while (tmpList.Count == 0);

		string tmpString = "";
		do
		{
			tmpString = Izanagi.GetAnswerString(questionType, tmpList[Random.Range(0, tmpList.Count)]);
		}
		while (tmpString == "");

		string finalString = "";
		do
		{
			finalString = tmpString[Random.Range(0, tmpString.Length)].ToString();
		}
		while (finalString == string.Empty || finalString == "/");

		return finalString;
	}

	public string GetRandomAnswerCharacterKanas(QuestionType questionType, string FirstKana)
	{
		int start = 0, end = 0;

		switch (Izanagi.Instance.GameSettings.KanaType)
		{
			case KanaType.HIRAGANA:
				start = 0;
				end = 46;
				break;
			case KanaType.KATAKANA:
				start = 46;
				end = 92;
				break;
			case KanaType.BOTH:
				start = 0;
				end = 92;
				break;
		}

		string tmpString = "";
		Datas data = null;
		do
		{
			data = Kanas[Random.Range(start, end)];
			tmpString = Izanagi.GetAnswerString(questionType, data);
		}
		while (tmpString == "" || data == null || data.FirstKana == FirstKana);

		return tmpString;
	}

	public void UpdateVocabularyDatabaseWithData(Datas data)
	{
		IDbCommand IDbCommand = SqliteHelper.GetDbCommand();
		IDbCommand.CommandText = "UPDATE Vocabulary "
			+ "SET Exercices_Total = " + data.ExercicesStats.Total.ToString() + ", "
			+ "Exercices_Good = " + data.ExercicesStats.Good.ToString() + ", "
			+ "Exercices_Wrong = " + data.ExercicesStats.Wrong.ToString() + ", "
			+ "Exercices_Percent = " + data.ExercicesStats.Percent.ToString().Replace(',', '.') + ", "
			+ "Exercices_Average_Time = " + data.ExercicesStats.AverageTime.ToString().Replace(',','.') + ", "
			+ "Tests_Total = " + data.TestsStats.Total.ToString() + ", "
			+ "Tests_Good = " + data.TestsStats.Good.ToString() + ", "
			+ "Tests_Wrong = " + data.TestsStats.Wrong.ToString() + ", "
			+ "Tests_Percent = " + data.TestsStats.Percent.ToString().Replace(',', '.') + ", "
			+ "Tests_Average_Time = " + data.TestsStats.AverageTime.ToString().Replace(',', '.') + " "
			+ "WHERE Kanji = '" + Izanagi.FormatMultipleStringWithSlash(data.Kanji) + "' AND "
			+ "Kana = '" + Izanagi.FormatMultipleStringWithSlash(data.Kana) + "' AND "
			+ "Romaji = '" + Izanagi.FormatMultipleStringWithSlash(data.Romaji) + "'";

		IDbCommand.ExecuteNonQuery();
	}

	public void UpdateKanasDatabaseWithData(Datas data)
	{
		IDbCommand IDbCommand = SqliteHelper.GetDbCommand();
		IDbCommand.CommandText = "UPDATE Kanas "
			+ "SET Exercices_Total = " + data.ExercicesStats.Total.ToString() + ", "
			+ "Exercices_Good = " + data.ExercicesStats.Good.ToString() + ", "
			+ "Exercices_Wrong = " + data.ExercicesStats.Wrong.ToString() + ", "
			+ "Exercices_Percent = " + data.ExercicesStats.Percent.ToString().Replace(',', '.') + ", "
			+ "Exercices_Average_Time = " + data.ExercicesStats.AverageTime.ToString().Replace(',', '.') + " "
			+ "WHERE Kana = '" + Izanagi.FormatMultipleStringWithSlash(data.Kana) + "' AND "
			+ "Romaji = '" + Izanagi.FormatMultipleStringWithSlash(data.Romaji) + "'";

		IDbCommand.ExecuteNonQuery();
	}
}
