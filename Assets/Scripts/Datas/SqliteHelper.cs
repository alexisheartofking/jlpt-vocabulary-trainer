﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Data.Sqlite;
using UnityEngine;
using System.Data;
using System.IO;
using UnityEngine.Networking;

public class SqliteHelper
{
	private const string LogTag = "[Sqlite Helper] ";

	private const string database_name = "Database.db";

	public string db_connection_string;
	public IDbConnection db_connection;

	public SqliteHelper()
	{
#if UNITY_ANDROID

		string filepath = Application.persistentDataPath + "/" + database_name;
		if (!File.Exists(filepath))
		{
			Debug.Log(Application.dataPath + "!/assets/" + database_name);
			UnityWebRequest loadDB = UnityEngine.Networking.UnityWebRequest.Get(Application.streamingAssetsPath + "/" + database_name);
			loadDB.SendWebRequest();
			while (!loadDB.isDone)
			{
			}

			File.WriteAllBytes(filepath, loadDB.downloadHandler.data);
		}
		Debug.Log(filepath);
		db_connection_string = "URI=file:" + filepath;
#endif
#if UNITY_EDITOR
		db_connection_string = "URI=file:" + Application.streamingAssetsPath + "/" + database_name;
#endif
		Debug.Log(LogTag + "db_connection_string: " + db_connection_string);
		db_connection = new SqliteConnection(db_connection_string);
		db_connection.Open();
	}

	~SqliteHelper()
	{
		db_connection.Close();
	}

	public virtual IDataReader GetDataById(int id)
	{
		Debug.Log(LogTag + "This function is not implemnted");
		throw null;
	}

	public virtual IDataReader GetDataByString(string str)
	{
		Debug.Log(LogTag + "This function is not implemnted");
		throw null;
	}

	public virtual void DeleteDataById(int id)
	{
		Debug.Log(LogTag + "This function is not implemented");
		throw null;
	}

	public virtual void DeleteDataByString(string id)
	{
		Debug.Log(LogTag + "This function is not implemented");
		throw null;
	}

	public virtual IDataReader GetAllData()
	{
		Debug.Log(LogTag + "This function is not implemented");
		throw null;
	}

	public virtual void DeleteAllData()
	{
		Debug.Log(LogTag + "This function is not implemnted");
		throw null;
	}

	public virtual IDataReader GetNumOfRows()
	{
		Debug.Log(LogTag + "This function is not implemnted");
		throw null;
	}

	public IDbCommand GetDbCommand()
	{
		return db_connection.CreateCommand();
	}

	public IDataReader GetAllData(string tableName)
	{
		IDbCommand dbcmd = db_connection.CreateCommand();
		dbcmd.CommandText =
			"SELECT * FROM " + tableName;
		IDataReader reader = dbcmd.ExecuteReader();
		return reader;
	}

	public void DeleteAllData(string tableName)
	{
		IDbCommand dbcmd = db_connection.CreateCommand();
		dbcmd.CommandText = "DROP TABLE IF EXISTS " + tableName;
		dbcmd.ExecuteNonQuery();
	}

	public IDataReader GetNumOfRows(string tableName)
	{
		IDbCommand dbcmd = db_connection.CreateCommand();
		dbcmd.CommandText =
			"SELECT COALESCE(MAX(id)+1, 0) FROM " + tableName;
		IDataReader reader = dbcmd.ExecuteReader();
		return reader;
	}

	public void Close()
	{
		db_connection.Close();
	}
}