﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LearningVocabularyEntry : MonoBehaviour
{
	[SerializeField]
	private Text JPLTText;
	[SerializeField]
	private Text kanjiText;
	[SerializeField]
	private Text kanaText;
	[SerializeField]
	private Text meaningText;

	[SerializeField]
	private Datas data;

    public void Initialize(Datas InData)
    {
		data = InData;

		JPLTText.text = InData.JLPT.ToString();
		kanjiText.text = Izanagi.FormatMultipleStringBreakLine(InData.Kanji);
		kanaText.text = Izanagi.FormatMultipleStringBreakLine(InData.Kana);
		meaningText.text = InData.GetCurrentTranslation();
	}
}
