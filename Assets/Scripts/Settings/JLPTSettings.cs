﻿using UnityEngine.UI;

public class JLPTSettings : MultipleChoiceSetting
{
	public override void AddSetting(Button Button)
	{
		Izanagi.Instance.GameSettings.JLPTLevels.Add(Button.GetComponent<JLPTSetting>().JLPTLevel);
	}

	public override void RemoveSetting(Button Button)
	{
		if (Izanagi.Instance.GameSettings.JLPTLevels.Contains(Button.GetComponent<JLPTSetting>().JLPTLevel))
			Izanagi.Instance.GameSettings.JLPTLevels.Remove(Button.GetComponent<JLPTSetting>().JLPTLevel);
	}
}
