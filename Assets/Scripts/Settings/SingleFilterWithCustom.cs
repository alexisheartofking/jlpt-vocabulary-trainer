﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleFilterWithCustom : MonoBehaviour
{
	[SerializeField]
	private List<Button> settingButtons;

	private bool isInitialized = false;
	private Button CurrentButton;

	public Text CurrentValue;
	public int baseValue = 4;

	public void Reset()
	{
		if (!isInitialized)
		{
			for (int i = 0; i < settingButtons.Count; ++i)
			{
				int index = i;
				settingButtons[i].onClick.AddListener(() => SettingSelected(settingButtons[index]));
			}
		}

		if (settingButtons.Count > baseValue)
			SettingSelected(settingButtons[baseValue]);
	}

	private void SettingSelected(Button Button)
	{
		if (CurrentButton != null)
			Izanagi.ButtonTransitionToNormal(CurrentButton);

		CurrentButton = Button;
		Izanagi.ButtonTransitionToSelected(CurrentButton);

		CurrentValue = CurrentButton.GetComponentInChildren<Text>();

		if (CurrentValue.text == "Custom")
			CurrentValue = CurrentButton.transform.parent.GetComponentInChildren<InputField>().textComponent;
	}
}
