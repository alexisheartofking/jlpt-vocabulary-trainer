﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionTypeSettings : MultipleChoiceSetting
{
	public override void AddSetting(Button Button)
	{
		Izanagi.Instance.GameSettings.QuestionTypes.Add(Button.GetComponent<QuestionTypeSetting>().QuestionType);
	}

	public override void RemoveSetting(Button Button)
	{
		if (Izanagi.Instance.GameSettings.QuestionTypes.Contains(Button.GetComponent<QuestionTypeSetting>().QuestionType))
			Izanagi.Instance.GameSettings.QuestionTypes.Remove(Button.GetComponent<QuestionTypeSetting>().QuestionType);
	}
}
