﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultipleChoiceSetting : MonoBehaviour
{
	[System.Serializable]
	public class ChoiceState
	{
		public Button Button;
		public bool IsSelected = false;
	}

	[SerializeField]
	private List<ChoiceState> settingButtons;

	private bool isInitialized = false;

	private int ButtonSelected = 0;

	public void Reset()
	{
		if (!isInitialized)
		{
			for (int i = 0; i < settingButtons.Count; ++i)
			{
				int index = i;
				settingButtons[i].Button.onClick.AddListener(() => SettingSelected(settingButtons[index]));
			}
		}

		for (int i = 0; i < settingButtons.Count; ++i)
		{
			if (settingButtons[i].IsSelected)
			{
				settingButtons[i].IsSelected = false;
				Izanagi.ButtonTransitionToNormal(settingButtons[i].Button);
				RemoveSetting(settingButtons[i].Button);
			}
		}

		ButtonSelected = 0;
	}

	private void SettingSelected(ChoiceState choiceState)
	{
		if (choiceState.Button.GetComponentInChildren<Text>().text == "All")
		{
			if (choiceState.IsSelected)
			{
				for (int i = 0; i < settingButtons.Count; ++i)
				{
					if (settingButtons[i].IsSelected)
					{
						settingButtons[i].IsSelected = false;
						Izanagi.ButtonTransitionToNormal(settingButtons[i].Button);

						if (i < settingButtons.Count - 1)
							RemoveSetting(settingButtons[i].Button);
					}
				}
				ButtonSelected = 0;
			}
			else
			{
				for (int i = 0; i < settingButtons.Count; ++i)
				{
					if (!settingButtons[i].IsSelected)
					{
						settingButtons[i].IsSelected = true;
						Izanagi.ButtonTransitionToSelected(settingButtons[i].Button);

						if (i < settingButtons.Count - 1)
							AddSetting(settingButtons[i].Button);
					}
				}
				ButtonSelected = settingButtons.Count - 1;
			}
		}
		else
		{
			if (choiceState.IsSelected)
			{
				choiceState.IsSelected = false;
				Izanagi.ButtonTransitionToNormal(choiceState.Button);
				RemoveSetting(choiceState.Button);
				--ButtonSelected;
			}
			else
			{
				choiceState.IsSelected = true;
				Izanagi.ButtonTransitionToSelected(choiceState.Button);
				AddSetting(choiceState.Button);
				++ButtonSelected;
			}

			if (ButtonSelected == settingButtons.Count - 1)
			{
				settingButtons[settingButtons.Count - 1].IsSelected = true;
				Izanagi.ButtonTransitionToSelected(settingButtons[settingButtons.Count - 1].Button);
			}
			else if (settingButtons[settingButtons.Count - 1].IsSelected)
			{
				settingButtons[settingButtons.Count - 1].IsSelected = false;
				Izanagi.ButtonTransitionToNormal(settingButtons[settingButtons.Count - 1].Button);
			}
		}
	}

	public void SelectAll()
	{
		for (int i = 0; i < settingButtons.Count; ++i)
		{
			if (!settingButtons[i].IsSelected)
			{
				settingButtons[i].IsSelected = true;
				Izanagi.ButtonTransitionToSelected(settingButtons[i].Button);

				if (i < settingButtons.Count - 1)
					AddSetting(settingButtons[i].Button);
			}
		}
		ButtonSelected = settingButtons.Count - 1;
	}

	public void UnselectAll()
	{
		for (int i = 0; i < settingButtons.Count; ++i)
		{
			if (settingButtons[i].IsSelected)
			{
				settingButtons[i].IsSelected = false;
				Izanagi.ButtonTransitionToNormal(settingButtons[i].Button);

				if (i < settingButtons.Count - 1)
					RemoveSetting(settingButtons[i].Button);
			}
		}
		ButtonSelected = 0;
	}

	public virtual void AddSetting(Button button)
	{
	}

	public virtual void RemoveSetting(Button button)
	{
	}

}
