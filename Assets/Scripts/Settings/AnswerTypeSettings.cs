﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerTypeSettings : MultipleChoiceSetting
{
	public override void AddSetting(Button Button)
	{
		Izanagi.Instance.GameSettings.AnswerTypes.Add(Button.GetComponent<AnswerTypeSetting>().AnswerType);
	}

	public override void RemoveSetting(Button Button)
	{
		if (Button.GetComponent<AnswerTypeSetting>() && Izanagi.Instance.GameSettings.AnswerTypes.Contains(Button.GetComponent<AnswerTypeSetting>().AnswerType))
			Izanagi.Instance.GameSettings.AnswerTypes.Remove(Button.GetComponent<AnswerTypeSetting>().AnswerType);
	}
}
