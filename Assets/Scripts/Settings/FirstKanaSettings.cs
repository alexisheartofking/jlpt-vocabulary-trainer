﻿using UnityEngine.UI;

public class FirstKanaSettings : MultipleChoiceSetting
{
	public override void AddSetting(Button Button)
	{
		Izanagi.Instance.GameSettings.FirstKanas.Add(Button.GetComponent<FirstKanaSetting>().FirstKana);
	}

	public override void RemoveSetting(Button Button)
	{
		if (Izanagi.Instance.GameSettings.FirstKanas.Contains(Button.GetComponent<FirstKanaSetting>().FirstKana))
			Izanagi.Instance.GameSettings.FirstKanas.Remove(Button.GetComponent<FirstKanaSetting>().FirstKana);
	}
}
