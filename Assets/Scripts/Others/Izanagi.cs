﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Izanagi : MonoBehaviour
{
	private static Izanagi instance = null;

	public static Izanagi Instance
	{
		get
		{
			if (instance == null)
				instance = GameObject.FindObjectOfType<Izanagi>();

			return instance;
		}
	}

	[Header("Game Settings")]
	public GameSettings GameSettings;

	public SettingsMenu.Type SettingsType;

	[Header("Menus")]
	[SerializeField]
	private Menu MainMenu;
	[SerializeField]
	private Menu LearningMenu;
	[SerializeField]
	private Menu LearningVocabularyMenu;
	[SerializeField]
	private Menu LearningKanasMenu;
	[SerializeField]
	private Menu SettingsMenu;
	[SerializeField]
	private Menu GameMenu;
	[SerializeField]
	private Menu RecapMenu;

	private Dictionary<string, Menu> Menus;
	private Menu CurrentMenu;
	private float MenuTransitionTime = 0.15f;
	private bool inTransition = false;

	public MyDictionary MyDictionary;

	[Header("Popups")]
	public DetailsPanelHandler DetailsPanelHandler;
	public ErrorPanelHandler ErrorPanelHandler;

	public void Awake()
	{
		GameSettings = new GameSettings();
		MyDictionary = new MyDictionary();
		Menus = new Dictionary<string, Menu>();

		Menus.Add("mainmenu", MainMenu);
		Menus.Add("settingsmenu", SettingsMenu);
		Menus.Add("gamemenu", GameMenu);
		Menus.Add("recapmenu", RecapMenu);
		Menus.Add("learningmenu", LearningMenu);
		Menus.Add("learningvocabularymenu", LearningVocabularyMenu);
		Menus.Add("learningkanasmenu", LearningKanasMenu);

		CurrentMenu = MainMenu;
	}

	public void ChangeMenu(string MenuName)
	{
		if (!inTransition)
			StartCoroutine(ChangeMenuRoutine(MenuName));
	}

	private IEnumerator ChangeMenuRoutine(string MenuName)
	{
		inTransition = true;

		float timer = 0f;
		while (timer <= MenuTransitionTime)
		{
			float percent = 1f - timer / MenuTransitionTime;
			CurrentMenu.CanvasGroup.alpha = CurrentMenu.fadeOut.Evaluate(percent);

			yield return null;

			timer += Time.deltaTime;
		}

		CurrentMenu.CanvasGroup.alpha = 0f;

		CurrentMenu.Close();
		CurrentMenu.gameObject.SetActive(false);

		Menu oldMenu = CurrentMenu;
		CurrentMenu = Menus[MenuName.ToLower()];

		CurrentMenu.Open();
		CurrentMenu.gameObject.SetActive(true);

		timer = 0f;
		while (timer <= MenuTransitionTime)
		{
			float percent = timer / MenuTransitionTime;
			CurrentMenu.CanvasGroup.alpha = CurrentMenu.fadeOut.Evaluate(percent);

			yield return null;

			timer += Time.deltaTime;
		}

		CurrentMenu.CanvasGroup.alpha = 1f;

		CurrentMenu.LateOpen();
		oldMenu.LateClose();

		inTransition = false;
	}

	/**************************************/
	/*************** HELPER ***************/
	/**************************************/

	public static void ButtonTransitionToSelected(Button button)
	{
		ColorBlock colors = button.colors;
		colors.normalColor = colors.selectedColor;
		button.colors = colors;
	}

	public static void ButtonTransitionToNormal(Button button)
	{
		ColorBlock colors = button.colors;
		colors.normalColor = Color.white;
		button.colors = colors;
	}

	public static string FormatMultipleStringBreakLine(List<string> list)
	{
		string finalString = "";

		for (int i = 0; i < list.Count; ++i)
		{
			if (i > 0)
				finalString += "\n";

			finalString += list[i];
		}

		return finalString;
	}

	public static string FormatMultipleStringWithSlash(List<string> list)
	{
		string finalString = "";

		for (int i = 0; i < list.Count; ++i)
		{
			if (i > 0)
				finalString += "/";

			finalString += list[i];
		}

		return finalString;
	}

	public static string GetRealAnswerString(QuestionType questionType, Datas data)
	{
		switch (questionType)
		{
			case QuestionType.KANA_TO_KANJI:
			case QuestionType.ROMAJI_TO_KANJI:
			case QuestionType.ENGLISH_TO_KANJI:
				return data.Kanji[Random.Range(0, data.Kanji.Count)];
			case QuestionType.KANJI_TO_KANA:
			case QuestionType.ROMAJI_TO_KANA:
			case QuestionType.ENGLISH_TO_KANA:
				return data.Kana[Random.Range(0, data.Kana.Count)];
			case QuestionType.KANJI_TO_ROMAJI:
			case QuestionType.KANA_TO_ROMAJI:
			case QuestionType.ENGLISH_TO_ROMAJI:
				return data.Romaji[Random.Range(0, data.Romaji.Count)];
			case QuestionType.KANJI_TO_ENGLISH:
			case QuestionType.KANA_TO_ENGLISH:
			case QuestionType.ROMAJI_TO_ENGLISH:
				return data.GetCurrentTranslation();
			default:
				break;
		}

		return "";
	}

	public static string GetAnswerString(QuestionType questionType, Datas data)
	{
		switch (questionType)
		{
			case QuestionType.KANA_TO_KANJI:
			case QuestionType.ROMAJI_TO_KANJI:
			case QuestionType.ENGLISH_TO_KANJI:
				return Izanagi.FormatMultipleStringWithSlash(data.Kanji);
			case QuestionType.KANJI_TO_KANA:
			case QuestionType.ROMAJI_TO_KANA:
			case QuestionType.ENGLISH_TO_KANA:
				return Izanagi.FormatMultipleStringWithSlash(data.Kana);
			case QuestionType.KANJI_TO_ROMAJI:
			case QuestionType.KANA_TO_ROMAJI:
			case QuestionType.ENGLISH_TO_ROMAJI:
				return Izanagi.FormatMultipleStringWithSlash(data.Romaji);
			case QuestionType.KANJI_TO_ENGLISH:
			case QuestionType.KANA_TO_ENGLISH:
			case QuestionType.ROMAJI_TO_ENGLISH:
				return data.GetCurrentTranslation();
			default:
				break;
		}

		return "";
	}

	public static List<string> GetAnswers(QuestionType questionType, Datas data)
	{
		switch (questionType)
		{
			case QuestionType.KANA_TO_KANJI:
			case QuestionType.ROMAJI_TO_KANJI:
			case QuestionType.ENGLISH_TO_KANJI:
				return data.Kanji;
			case QuestionType.KANJI_TO_KANA:
			case QuestionType.ROMAJI_TO_KANA:
			case QuestionType.ENGLISH_TO_KANA:
				return data.Kana;
			case QuestionType.KANJI_TO_ROMAJI:
			case QuestionType.KANA_TO_ROMAJI:
			case QuestionType.ENGLISH_TO_ROMAJI:
				return data.Romaji;
			case QuestionType.KANJI_TO_ENGLISH:
			case QuestionType.KANA_TO_ENGLISH:
			case QuestionType.ROMAJI_TO_ENGLISH:
				List<string> tmp = new List<string>();
				tmp.Add(data.GetCurrentTranslation());
				return tmp;
			default:
				break;
		}

		return new List<string>();
	}

	public static string GetQuestionString(QuestionType questionType, Datas data)
	{
		switch (questionType)
		{
			case QuestionType.KANJI_TO_KANA:
			case QuestionType.KANJI_TO_ROMAJI:
			case QuestionType.KANJI_TO_ENGLISH:
				return Izanagi.FormatMultipleStringBreakLine(data.Kanji);
			case QuestionType.KANA_TO_KANJI:
			case QuestionType.KANA_TO_ROMAJI:
			case QuestionType.KANA_TO_ENGLISH:
				return Izanagi.FormatMultipleStringBreakLine(data.Kana);
			case QuestionType.ROMAJI_TO_KANJI:
			case QuestionType.ROMAJI_TO_KANA:
			case QuestionType.ROMAJI_TO_ENGLISH:
				return Izanagi.FormatMultipleStringBreakLine(data.Romaji);
			case QuestionType.ENGLISH_TO_KANJI:
			case QuestionType.ENGLISH_TO_KANA:
			case QuestionType.ENGLISH_TO_ROMAJI:
				return data.GetCurrentTranslation();
			default:
				break;
		}

		return "";
	}
}
