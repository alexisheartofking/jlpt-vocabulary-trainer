﻿using System.Collections.Generic;

public enum JLPTLevel
{
	N1 = 1,
	N2 = 2,
	N3 = 3,
	N4 = 4,
	N5 = 5
};

public enum AnswerType
{
	KEYBOARD,
	DUAL_CHOICES,
	QUARTET_CHOICES
};

public enum QuestionType
{
	KANJI_TO_KANA,
	KANJI_TO_ROMAJI,
	KANJI_TO_ENGLISH,
	KANA_TO_KANJI,
	KANA_TO_ROMAJI,
	KANA_TO_ENGLISH,
	ROMAJI_TO_KANJI,
	ROMAJI_TO_KANA,
	ROMAJI_TO_ENGLISH,
	ENGLISH_TO_KANJI,
	ENGLISH_TO_KANA,
	ENGLISH_TO_ROMAJI
};

public enum KanaType
{
	HIRAGANA,
	KATAKANA,
	BOTH
}

[System.Serializable]
public class GameSettings
{
	public int QuestionCount = 0;
	public int TotalTimer = 0;
	public bool UseTimer = false;

	public List<JLPTLevel> JLPTLevels;
	public List<AnswerType> AnswerTypes;
	public List<QuestionType> QuestionTypes;
	public List<string> FirstKanas;
	public KanaType KanaType;

	public List<Datas> AllDatas;
	public List<RecapData> AllRecapDatas;

	public GameSettings()
	{
		QuestionCount = 0;
		TotalTimer = 0;
		UseTimer = false;

		JLPTLevels = new List<JLPTLevel>();
		AnswerTypes = new List<AnswerType>();
		QuestionTypes = new List<QuestionType>();
		FirstKanas = new List<string>();

		AllDatas = new List<Datas>();
	}

	public bool IsCompatibleQuestionsAndAnswers()
	{
		if (!AnswerTypes.Contains(AnswerType.KEYBOARD))
			return true;

		if (QuestionTypes.Count > 3)
			return true;

		for (int i = 0; i < QuestionTypes.Count; ++i)
		{
			if (QuestionTypes[i] != QuestionType.KANJI_TO_ENGLISH && QuestionTypes[i] != QuestionType.KANA_TO_ENGLISH && QuestionTypes[i] != QuestionType.ROMAJI_TO_ENGLISH)
				return true;
		}

		return false;
	}

	public bool IsCompatibleQuestionsAndAnswer()
	{
		if (AnswerTypes.Count == 1 && AnswerTypes[0] == AnswerType.KEYBOARD)
		{
			if (QuestionTypes.Contains(QuestionType.KANJI_TO_ENGLISH) || QuestionTypes.Contains(QuestionType.KANA_TO_ENGLISH) || QuestionTypes.Contains(QuestionType.ROMAJI_TO_ENGLISH))
				return false;
		}

		return true;
	}
}
