﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecapEntry : MonoBehaviour
{
	[SerializeField]
	private Text questionText;
	[SerializeField]
	private Text answerText;
	[SerializeField]
	private Text yourAnswerText;

	private RecapData recapData;

	public void Initialize(RecapData inRecapData)
	{
		recapData = inRecapData;

		questionText.text = recapData.question;
		answerText.text = recapData.answer;
		yourAnswerText.text = recapData.yourAnswer;

		if (recapData.hasWon)
			GetComponent<Image>().color = new Color(113f / 255f, 1f, 0f);
		else
			GetComponent<Image>().color = new Color(1f, 65f / 255f, 0f);

		GetComponent<Button>().onClick.AddListener(() => Izanagi.Instance.DetailsPanelHandler.Open(inRecapData.data, true));
	}
}
